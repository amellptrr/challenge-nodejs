class Car {
  constructor(cars) {
    this.cars = cars;
  }
  filterCarAvailable() {
    return this.cars.filter((car) => car.available === true);
  }
  filterCarByUser() {
    var driver = document.getElementById("driver").value;
    var date = document.getElementById("date").value;
    var time = document.getElementById("time").value;
    var dateTime = date + time;
    var passanger = document.getElementById("passanger").value;

    if (driver === undefined) {
      alert("Pilih Tipe Driver!");
      return;
    } else if (dateTime < getDateTimeNow()) {
      alert("Pilih Tanggal dan Waktu yang tepat!");
      return;
    } else if (passanger == "") {
      return this.cars.filter(
        (car) => car.available === true && car.availableAt <= dateTime
      );
    } else {
      return this.cars.filter(
        (car) =>
          car.available === true &&
          car.capacity >= passanger &&
          car.availableAt <= dateTime
      );
    }
  }
}

var xmlHttp = new XMLHttpRequest();
xmlHttp.open("GET", "http://localhost:8000/api/cars", false);
xmlHttp.send(null); // Request body null

var data = JSON.parse(xmlHttp.responseText); // Data dari JSON
var cars = new Car(data); // Filter Car by Available
var app = document.getElementById("carsList");
htmlData = "";

data = cars.filterCarAvailable();

function rupiah(number) {
  return new Intl.NumberFormat("id-ID", {
    style: "currency",
    currency: "IDR",
  }).format(number);
}

var btnFilterCar = document
  .getElementById("btnFilterCar")
  .addEventListener("click", getCars);

function getDateTimeNow() {
  var today = new Date();
  var date =
    today.getFullYear() +
    "-" +
    String(today.getMonth() + 1).padStart(2, "0") +
    "-" +
    String(today.getDate()).padStart(2, "0");
  var time =
    String(today.getHours()).padStart(2, "0") +
    ":" +
    String(today.getMinutes()).padStart(2, "0") +
    ":" +
    String(today.getSeconds()).padStart(2, "0");
  var dateNow = date + "T" + time + ".000Z";
  return dateNow;
}

function getCars() {
  var htmlData = "";
  data = cars.filterCarByUser();
  if (data === undefined) {
    alert("Car Not Found");
  } else {
    for (let index = 0; index < data.length; index++) {
      var car = data[index];
      var rentCost = rupiah(car.rentPerDay);
      htmlData += `
            <div class="col-auto m-2">
                <div class="card" style="width: 18rem; height: 550px; margin-left:50px">
                <img src="${car.image}"" class="card-img-top img-fluid" alt="${car.manufacture}" style="height: 190px; width:100%; object-fit: scale-down;">
                <div class="card-body" style="font-size: 14px;">
                    <p class="card-title">${car.manufacture} ${car.model}</p>
                    <p class="fw-bold">${rentCost} / hari</p>
                    <p class="card-text" style="height: 90px">${car.description}</p>
                    <div class="my-2"><i class="bi bi-people me-2"></i>${car.capacity} Orang</div>
                    <div class="my-2"><i class="bi bi-gear me-2"></i>${car.transmission}</div>
                    <div class="my-2"><i class="bi bi-calendar4 me-2"></i>${car.year}</div>
                    <a href="#" class="btn bg-button text-white w-100 mt-2 fw-bold mt-4" style="font-size: 14px;">Pilih Mobil</a>
                </div>
                </div>
            </div>
            `;
    }
    app.innerHTML = htmlData;
  }
}
